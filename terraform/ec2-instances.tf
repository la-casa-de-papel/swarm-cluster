data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "manager" {
  count           = var.manager_count
  ami             = data.aws_ami.ubuntu.id
  instance_type   = var.manager_instance_type
  key_name        = var.provisioner_key_name
  security_groups = [aws_security_group.main.name]
  tags = {
    Name          = join("_", [var.environment, "manager", count.index])
    Environment   = var.environment
  }
}

resource "aws_instance" "worker" {
  count           = var.worker_count
  ami             = data.aws_ami.ubuntu.id
  instance_type   = var.worker_instance_type
  key_name        = var.provisioner_key_name
  security_groups = [aws_security_group.main.name]
  tags = {
    Name          = join("_", [var.environment, "worker", count.index])
    Environment   = var.environment
  }
}

output "manager_hosts" {
  value = join("\n", aws_instance.manager.*.public_ip)
}

output "worker_hosts" {
  value = join("\n", aws_instance.worker.*.public_ip)
}

output "public_ip" {
  value = aws_instance.manager[0].public_ip
}

locals {
  ingress_rules = {
    22   = { description = "For server administration", cidr_blocks = [ "0.0.0.0/0" ] }
    80   = { description = "For web API",               cidr_blocks = [ "0.0.0.0/0" ] }
    8080 = { description = "For Adminer",               cidr_blocks = [ "0.0.0.0/0" ] }
  }
}

resource "aws_security_group" "main" {
  name        = var.environment
  description = join(" ", ["Inbound rules for", var.environment, "environment"])
  dynamic "ingress" {
    for_each = local.ingress_rules
    content {
      description = ingress.value["description"]
      from_port   = ingress.key
      to_port     = ingress.key
      protocol    = "tcp"
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Environment = var.environment
  }
}
