variable "environment" {
  type = string
}

variable "is_production" {
  type = bool
}

variable "manager_count" {
  type    = number
  default = 1
}

variable "worker_count" {
  type    = number
  default = 0
}

variable "manager_instance_type" {
  type    = string
  default = "t2.micro"
}

variable "worker_instance_type" {
  type    = string
  default = "t2.micro"
}

variable "provisioner_key_name" {
  type = string
}

variable "domains" {
  type = list(string)
  default = []
}

variable "subdomains" {
  type = list(object({
    domain = string
    subdomain = string
  }))
  default = []
}
