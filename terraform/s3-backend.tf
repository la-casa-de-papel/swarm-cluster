terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "ada-baiana"
    key    = "tfstate"
  }
}
